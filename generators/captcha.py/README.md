# ENV
## Using Virtualenv:
```bash
	mkdir env
	virtualenv -p python3 env
	. env/bin/activate
	pip install -r requirements.txt
```
## Using Conda:
```bash
	conda create -n captcha captcha=0.4
	conda activate captcha
```
# RUN
```bash
	# Makes the destination directory if it already does not exists

	mkdir -p out

	# Generates 2000 random captcha images of 6 numbers, lower-case and/or upper-case characters, randomizing its fonts in the 'out' directory
	python captcha_gen.py      \
		--font             \
		--output out       \
		--length 6         \
		--alphabet Ana     \
		--threads $(nproc) \
		--number 2000      
```

# DOC
See output of `python captcha_gen.py --help`
