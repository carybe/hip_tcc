#!/usr/bin/env python3

import os
import sys
from optparse import OptionParser
from captcha.image import ImageCaptcha
import multiprocessing
import string
import random

def run_in_thread(object):
	count = object[0];
	output_dir = object[1]
	length = object[2]
	characters = object[3]
	fonts = object[4]

	for i in range(count):
		word = ''.join(random.choices(characters, k=length))
		id = ''.join(random.choices(string.ascii_letters, k=6))
		filename = "%s_%s.png" % (word, id)
		output = os.path.join(output_dir, filename)

		image = ImageCaptcha(fonts=fonts, width=w*length, height=75)
		# data = image.generate(word)
		image.write(word, output)

if __name__ == '__main__':
	script_dir = os.path.dirname(os.path.realpath(__file__))
	parser = OptionParser()
	# parser.add_option("--text", "-t", help="A text to be turned to a captcha (required)", metavar="TEXT")
	parser.add_option("--threads", "-t", help="Threads to be used for multiprocessing", metavar="TEXT")
	parser.add_option("--font", "-f", help="Use different, predefined, fonts", action='store_true')
	parser.add_option("--output", "-o", help="The directory to put the images in (required)", metavar="DIR")
	parser.add_option("--wheezy", "-w", help="Generate captcha with wheezy.captcha", action='store_true')
	parser.add_option("--length", "-l", help="Length of the word to be turned into a captcha (required)", metavar="LENGTH")
	parser.add_option("--alphabet", "-a", help="Alphabet to be used for a random generation of the captcha (required)", metavar="ALPHABET")
	parser.add_option("--number", "-n", help="Number of captchas to be generated", metavar="NUMBER")

	opts, args = parser.parse_args()

	# if opts.text:
	# 	text = opts.text
	# else:
	# 	sys.exit("Need to specify a text to be turned to a captcha")

	if opts.threads:
		threads = int(opts.threads)
	else:
		threads = multiprocessing.cpu_count()


	if opts.output and os.path.exists(opts.output):
		output_dir = opts.output
	else:
		sys.exit("Need to specify an output directory or the directory was not found")

	if opts.wheezy:
		from captcha.image import WheezyCaptcha as ImageCaptcha

	if not opts.length:
		sys.exit("Please, supply a length for the randomly generated captchas")
	length = int(opts.length)

	if not opts.alphabet:
		sys.exit("Please, supply a valid alphabet identifier for the randomly generated captchas [Ana]")


	characters = ''
	if "A" in opts.alphabet:
		characters += string.ascii_uppercase

	if "a" in opts.alphabet:
		characters += string.ascii_lowercase

	if "n" in opts.alphabet:
		characters += string.digits

	if not characters:
		sys.exit("The alphabet identifiers you supplied are invalid, please, use one of the following: [Ana]")

	if not opts.number:
		number = 1
	else:
		number = int(opts.number)
		if number <= 0:
			sys.exit("The number of captchas must be greater than 0")
	w=36
	fonts = None
	if opts.font:
		font_home="/home/gradmac/carybe/fonts/"
		fonts=[ "DejaVuSansMono.ttf",
				"DejaVuSans.ttf",
				"DejaVuSerif.ttf",
				"FreeMono.ttf",
				"FreeSans.ttf",
				"FreeSerif.ttf",
				"LiberationMono-Regular.ttf",
				"LiberationSans-Regular.ttf",
				"LiberationSerif-Regular.ttf",
				"NotoMono-Regular.ttf",
				"NotoSansMono-Regular.ttf",
				"NotoSans-Regular.ttf",
				"NotoSerif-Regular.ttf"]
		fonts=[font_home + font for font in fonts]

	if number < threads:
		chunks = 1
		threads = 1
	else:
		chunks = (number // threads)

	p = multiprocessing.Pool(threads);
	data = []
	print("Generating %s CAPTCHA images separated in %s image(s) per chunk run by %s threads..." % (number, chunks, threads))
	for i in range(0, threads):
		data.append([chunks, output_dir, length, characters, fonts])

	p.map(run_in_thread, data)

	remaining = number - chunks*threads
	if remaining > 0:
		data=[remaining, output_dir, length, characters, fonts]
		run_in_thread(data)
